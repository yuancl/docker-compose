使用这玩意的流程：1、安装docker->2、安装docker-compose->3、安装git

### 1、docker安装


```
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io

tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://ylto6llx.mirror.aliyuncs.com"]
}
EOF

systemctl daemon-reload
systemctl restart docker

开机自启动
systemctl enable docker
```

### 2、docker-compose安装

教程https://www.jianshu.com/p/6ac49f4c5a4e

下载：https://links.jianshu.com/go?to=https%3A%2F%2Fdownload.docker.com%2Flinux%2Fcentos%2F7%2Fx86_64%2Fstable%2FPackages%2Fdocker-compose-plugin-2.17.3-1.el7.x86_64.rpm

通过ssh或ftp上传该文件到服务器，执行


```

yum install docker-compose-plugin-2.17.3-1.el7.x86_64.rpm


执行这条可能报错，不需要理会
Loaded plugins: fastestmirror, ovl
Examining docker-compose-plugin-2.17.3-1.el7.x86_64.rpm: docker-compose-plugin-2.17.3-1.el7.x86_64
docker-compose-plugin-2.17.3-1.el7.x86_64.rpm: does not update installed package.
Error: Nothing to do




cd /usr/libexec/docker/cli-plugins

cp docker-compose /usr/local/bin
```

### 3、git安装

```
yum install -y git
```

### 4、比如elk

```
git clone https://gitee.com/zhengqingya/docker-compose.git
cd docker-compose/Linux/elk

docker-compose -f docker-compose-elk.yml -p elk up -d

chmod -R 777 ./elk
```









# docker-compose-linux

通过`docker-compose`编排一系列环境进行一键快速部署运行，小白运维神器。

### 一、环境准备

```shell
# 安装git命令： yum install -y git
git clone https://gitee.com/zhengqingya/docker-compose.git
cd docker-compose/Linux
```

### 二、运行服务

> 环境部署见每个服务下的`run.md`；
> eg: `Linux/portainer/run.md`

#### 数据库

1. [mysql](./Linux/mysql)
2. [mycat](./Linux/mycat)
3. [canal](./Linux/canal)
4. [oracle18c](./Linux/oracle18c)
5. [redis](./Linux/redis)
6. [couchbase](./Linux/couchbase)
7. [mongodb](./Linux/mongodb)
8. [seata](./Linux/seata)
9. [postgresql](./Linux/postgresql)
10. [yearning](./Linux/yearning)

#### 消息队列

1. [activemq](./Linux/activemq)
2. [kafka](./Linux/kafka)
3. [rabbitmq](./Linux/rabbitmq)
4. [rocketmq](./Linux/rocketmq)

#### 日志系统

1. [elk](./Linux/elk)
2. [efk](./Linux/efk)
3. [elkf](./Linux/elkf)
4. [filebeat](./Linux/filebeat)
5. [graylog](./Linux/graylog)
6. [grafana_promtail_loki](./Linux/grafana_promtail_loki)
7. [grafana-promtail-loki-nginx-demo](./Linux/grafana-promtail-loki-nginx-demo)
8. [plumelog](./Linux/plumelog)
9. [zipkin](./Linux/zipkin)

#### CI/CD

1. [portainer](./Linux/portainer)
2. [jenkins](./Linux/jenkins)
3. [rancher](./Linux/rancher)
4. [gitlab](./Linux/gitlab)
5. [gogs](./Linux/gogs)
6. [sonarqube](./Linux/sonarqube)
7. [prometheus](./Linux/prometheus)
8. [grafana](./Linux/grafana)
9. [nginx](./Linux/nginx)

#### 文件存储

1. [fastdfs](./Linux/fastdfs)
2. [minio](./Linux/minio)
3. [baidupcs-web](./Linux/baidupcs-web)
4. [nextcloud](./Linux/nextcloud)

#### 其它

1. [elasticsearch](./Linux/elasticsearch)
2. [flowable](./Linux/flowable)
3. [jrebel](./Linux/jrebel)
4. [jumpserver](./Linux/jumpserver)
5. [nacos](./Linux/nacos)
6. [nps](./Linux/nps)
7. [opensumi-web](./Linux/opensumi-web)
8. [sentinel](./Linux/sentinel)
9. [tomcat](./Linux/tomcat)
10. [walle](./Linux/walle)
11. [xxl-job](./Linux/xxl-job)
12. [PowerJob](./Linux/PowerJob)
13. [yapi](./Linux/yapi)
14. [zookeeper](./Linux/zookeeper)
15. [jpom](./Linux/jpom)
16. [confluence](./Linux/confluence)
17. [jira](./Linux/jira)
18. [skywalking](./Linux/skywalking)

---

### windows 使用 docker-compose 安装开发环境系列

> 见[windows 使用 docker-compose 安装开发环境系列.md](./windows使用docker-compose安装开发环境系列.md)

---

> 每天学习一点点，慢慢日积月累，你总会成为你喜欢的样子，加油。
